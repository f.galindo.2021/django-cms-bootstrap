from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from django.shortcuts import render
from django.utils import timezone
from .models import Contenido, Comentario
from django.contrib.auth import logout
from django.shortcuts import redirect
from .forms import ContenidoForm

# Definición de plantillas de formulario HTML para modificar contenido y agregar comentarios
formulario_contenido = """
<br>
<form action="" method="POST">
  Introduce el (nuevo) contenido para esta página: 
  <input type="text" name="valor">
  <input type="submit" name="action" value="Enviar Contenido">
</form>
"""

formulario_comentario = """
<br>
<form action="" method="POST">
  Introduce un nuevo comentario para esta página: 
  <br>Título: <input type="text" name="titulo">
  <br>Cuerpo: <input type="text" name="cuerpo">
  <input type="submit" name="action" value="Enviar Comentario">
</form>
"""

# Función de vista para modificar contenido existente
def cms_modify(request, llave):
    contenido = get_object_or_404(Contenido, clave=llave)  # Obtiene el contenido de la página con la clave dada
    if request.method == "POST":
        form = ContenidoForm(request.POST, instance=contenido)  # Crea un formulario con los datos de la solicitud y la instancia del contenido
        if form.is_valid():
            contenido = form.save()  # Guarda los cambios en el contenido
            return redirect('get_content', llave=contenido.clave)  # Redirige a la página de vista del contenido modificado
    else:
        form = ContenidoForm(instance=contenido)  # Crea un formulario con la instancia del contenido para mostrarlo
    return render(request, 'cms/cms_edit.html', {'form': form})  # Renderiza el formulario de edición de contenido

# Función de vista para crear nuevo contenido
def cms_new(request):
    if request.method == "POST":
        form = ContenidoForm(request.POST)  # Crea un formulario con los datos de la solicitud
        if form.is_valid():
            contenido = form.save()  # Guarda el nuevo contenido
            return redirect('get_content', llave=contenido.clave)  # Redirige a la página de vista del contenido nuevo
    else:
        form = ContenidoForm()  # Crea un formulario vacío para agregar nuevo contenido
    return render(request, 'cms/cms_edit.html', {'form': form})  # Renderiza el formulario de creación de contenido

# Función de vista para renderizar una plantilla
def imagen(request):
    template = loader.get_template('cms/plantilla.html')  # Obtiene la plantilla HTML
    context = {}  # Contexto vacío
    return HttpResponse(template.render(context, request))  # Renderiza la plantilla con el contexto

# Función de vista para cerrar sesión
def logout_view(request):
    logout(request)  # Cierra la sesión del usuario
    return redirect("/cms/")  # Redirige a la página principal del CMS

# Función de vista para verificar si el usuario está autenticado
def loggedIn(request):
    if request.user.is_authenticated:  # Verifica si el usuario está autenticado
        logged = "Logged in as " + request.user.username  # Mensaje para usuario autenticado
    else:
        logged = "Not logged in. <a href='/admin/'>Login via admin</a>"  # Mensaje para usuario no autenticado
    return HttpResponse(logged)  # Retorna el mensaje

# Función de vista para mostrar una lista de contenidos recientes
def index(request):
    content_list = Contenido.objects.all()[:5]  # Obtiene los últimos 5 contenidos
    context = {'content_list': content_list}  # Crea el contexto con la lista de contenidos
    return render(request, 'cms/index.html', context)  # Renderiza la plantilla "index.html" con el contexto

# Función de vista para obtener contenido de una página
@csrf_exempt  # Exención de protección CSRF
def get_content(request, llave):
    if request.method == "PUT":
        valor = request.body.decode('utf-8')  # Obtiene el contenido de la solicitud PUT
    elif request.method == "POST":
        action = request.POST['action']  # Obtiene la acción de la solicitud POST
        if action == "Enviar Contenido":
            valor = request.POST['valor']  # Obtiene el valor del contenido enviado
    if request.method == "PUT" or (request.method == "POST" and action == "Enviar Contenido"):
        try:
            c = Contenido.objects.get(clave=llave)  # Intenta obtener el contenido existente
            c.valor = valor  # Actualiza el valor del contenido
        except Contenido.DoesNotExist:
            c = Contenido(clave=llave, valor=valor)  # Si no existe, crea un nuevo contenido
        c.save()  # Guarda el contenido en la base de datos
    if request.method == "POST" and action == "Enviar Comentario":
        c = get_object_or_404(Contenido, clave=llave)  # Obtiene el contenido de la página
        titulo = request.POST['titulo']  # Obtiene el título del comentario
        cuerpo = request.POST['cuerpo']  # Obtiene el cuerpo del comentario
        q = Comentario(contenido=c, titulo=titulo, cuerpo=cuerpo, fecha=timezone.now())  # Crea un nuevo comentario
        q.save()  # Guarda el comentario en la base de datos

    contenido = get_object_or_404(Contenido, clave=llave)  # Obtiene el contenido de la página
    context = {'contenido': contenido}  # Crea el contexto con el contenido
    return render(request, 'cms/contenido.html', context)  # Renderiza la plantilla "contenido.html" con el contexto

'''

@csrf_exempt
def get_content(request, llave):
    if request.method == "PUT":
        valor = request.body.decode('utf-8')
    elif request.method == "POST":
        action = request.POST['action']
        if action == "Enviar Contenido":
            valor = request.POST['valor']
    if request.method == "PUT" or (request.method == "POST" and action == "Enviar Contenido"):
        try:
            c = Contenido.objects.get(clave=llave)
            c.valor = valor
        except Contenido.DoesNotExist:
            c = Contenido(clave=llave, valor=valor)
        c.save()
    if request.method == "POST" and action == "Enviar Comentario":
            c = get_object_or_404(Contenido, clave=llave)
            titulo = request.POST['titulo']
            cuerpo = request.POST['cuerpo']
            q = Comentario(contenido=c, titulo=titulo, cuerpo=cuerpo, fecha=timezone.now())
            q.save()

    contenido = get_object_or_404(Contenido, clave=llave)
    context = {'contenido': contenido}
    return render(request, 'cms/content.html', context)
'''


'''
@csrf_exempt
def get_content(request, llave):
    if request.method == "PUT":
        valor = request.body.decode('utf-8')
    elif request.method == "POST":
        action = request.POST['action']
        if action == "Enviar Contenido":
            valor = request.POST['valor']
    if request.method == "PUT" or (request.method == "POST" and action == "Enviar Contenido"):
        try:
            c = Contenido.objects.get(clave=llave)
            c.valor = valor
        except Contenido.DoesNotExist:
            c = Contenido(clave=llave, valor=valor)
        c.save()
    if request.method == "POST" and action == "Enviar Comentario":
            c = Contenido.objects.get(clave=llave)
            titulo = request.POST['titulo']
            cuerpo = request.POST['cuerpo']
            q = Comentario(contenido=c, titulo=titulo, cuerpo=cuerpo, fecha=timezone.now())
            q.save()

    contenido = get_object_or_404(Contenido, clave=llave)
    comentarios = contenido.comentario_set.all()
    respuesta = contenido.valor
    for comentario in comentarios:
        respuesta += "<p><b>Título</b>: " + comentario.titulo + "<br><b>Cuerpo</b>: " + comentario.cuerpo + "<br><b>Enviado</b>: " + comentario.fecha.strftime('%Y-%m-%d %H:%M:%S') + "</p>"
    respuesta += formulario_comentario
    return HttpResponse(respuesta + formulario_contenido)
'''


'''
    def get_content(request, llave):
        contenido = get_object_or_404(Contenido, clave=llave)
        return HttpResponse(contenido.valor)
'''


'''
    def get_content(request, llave):

        try:
            respuesta = Contenido.objects.get(clave=llave).valor
        except Contenido.DoesNotExist:
            respuesta = 'No existe contenido para la clave ' + llave
        return HttpResponse(respuesta)
    '''