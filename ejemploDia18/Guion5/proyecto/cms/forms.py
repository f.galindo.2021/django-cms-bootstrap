from django import forms  # Importa la clase forms de Django

from .models import Contenido  # Importa el modelo Contenido del mismo directorio

class ContenidoForm(forms.ModelForm):  # Define una clase de formulario para el modelo Contenido

    class Meta:  # Define la clase Meta para el formulario
        model = Contenido  # Especifica el modelo asociado al formulario
        fields = ('clave', 'valor',)  # Especifica los campos del modelo que se incluirán en el formulario



