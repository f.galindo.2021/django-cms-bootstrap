from django.urls import path  # Importa la función path de Django para definir rutas URL

from . import views  # Importa las vistas definidas en el módulo actual

# Define las rutas URL y las vincula a las vistas correspondientes

urlpatterns = [
    path('modify/<str:llave>', views.cms_modify, name="cms_modify"),  # Ruta para modificar contenido con una clave dada
    path('new/', views.cms_new, name='cms_new'),  # Ruta para crear nuevo contenido
    path('', views.index),  # Ruta para la página principal, muestra los últimos contenidos
    path('imagen', views.imagen),  # Ruta para mostrar una imagen, se espera que haya una vista asociada
    path('loggedIn', views.loggedIn),  # Ruta para verificar si el usuario está autenticado
    path('logout', views.logout_view),  # Ruta para cerrar sesión
    path('<str:llave>', views.get_content, name="get_content"),  # Ruta para obtener contenido por clave
]
