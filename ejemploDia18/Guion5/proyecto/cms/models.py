from django.db import models  # Importa el módulo de modelos de Django

# Define los modelos de la base de datos

class Contenido(models.Model):  # Define el modelo Contenido
    clave = models.CharField(max_length=64)  # Campo para la clave del contenido
    valor = models.TextField()  # Campo para el valor del contenido

    def __str__(self):  # Método para representar el objeto como una cadena
        return self.clave  # Devuelve la clave como representación de cadena

    def tiene_as(self):  # Método personalizado para verificar si el valor contiene la letra 'a'
        return ('a' in self.valor)  # Devuelve True si 'a' está en el valor, False en caso contrario

class Comentario(models.Model):  # Define el modelo Comentario
    contenido = models.ForeignKey(Contenido, on_delete=models.CASCADE)  # Relación muchos a uno con el modelo Contenido
    titulo = models.CharField(max_length=200)  # Campo para el título del comentario
    cuerpo = models.TextField(blank=False)  # Campo para el cuerpo del comentario, no puede estar en blanco
    fecha = models.DateTimeField('publicado')  # Campo para la fecha de publicación del comentario

    def __str__(self):  # Método para representar el objeto como una cadena
        return self.titulo  # Devuelve el título como representación de cadena
