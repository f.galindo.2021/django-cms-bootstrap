"""
URL configuration for proyecto project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin  # Importa el módulo de administración de Django
from django.urls import include, path  # Importa la función include y path de Django para definir rutas URL
from django.contrib.auth.views import LoginView as login  # Importa la vista de inicio de sesión predeterminada de Django
from django.conf import settings  # Importa la configuración del proyecto

from django.conf.urls.static import static  # Importa la función static para servir archivos estáticos

# Define las rutas URL para la aplicación y el panel de administración de Django

urlpatterns = [
    path('cms/', include('cms.urls')),  # Ruta para las URLs de la aplicación CMS, incluye las URLs definidas en cms.urls
    path("admin/", admin.site.urls),  # Ruta para el panel de administración de Django
    path('login', login.as_view()),  # Ruta para la vista de inicio de sesión, utilizando la vista predeterminada de Django
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)  # Agrega la configuración para servir archivos estáticos
